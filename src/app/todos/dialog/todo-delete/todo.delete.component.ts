import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { TodosService } from '../../todos.service';
import { TodosInterface } from '../../todos.interface.component';

@Component({
  selector: 'app-todo.delete',
  templateUrl: './todo.delete.component.html',
  styleUrls: ['./todo.delete.component.css']
})
export class TodoDeleteComponent implements OnInit {

  public todo: TodosInterface;
  public isDeleted: string;

  constructor(
    private todosService: TodosService,
    private dialogRef: MatDialogRef<TodoDeleteComponent>,
    @Inject(MAT_DIALOG_DATA) data) {
        if (data) {
            this.todo = data.todo;
        }
    }

  close(): void {
    this.dialogRef.close();
  }

  delete(id: number): void {
    this.isDeleted = 'processing';

    this.todosService.deleteTodo(id).subscribe((response: Response) => {
      this.isDeleted = 'delete';
      this.dialogRef.close(true);
    });
  }

  ngOnInit() {
  }

}
