import { Component, OnInit, Inject } from '@angular/core';
import { TodosService } from '../../todos.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { TodosInterface } from '../../todos.interface.component';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-todo.edit',
  templateUrl: './todo.edit.component.html',
  styleUrls: ['./todo.edit.component.css']
})
export class TodoEditComponent implements OnInit {

  public todo: TodosInterface;
  public editForm: FormGroup;
  public isUpdated: string;

  constructor(
    private todosService: TodosService,
    private dialogRef: MatDialogRef<TodoEditComponent>,
    @Inject(MAT_DIALOG_DATA) data) {
        if (data) {
            this.todo = data.todo;
        }
    }

  close(): void {
    this.dialogRef.close();
  }

  save(): void {
    this.isUpdated = 'processing';

    this.todosService.editTodo(this.todo.id, this.editForm.value).subscribe((response: Response) => {
      this.isUpdated = 'update';
      this.dialogRef.close(true);
    });
  }

  ngOnInit() {
    this.editForm = new FormGroup({
      'title': new FormControl(this.todo.title, Validators.required),
      'description': new FormControl(this.todo.description, Validators.required)
    });
  }

}
