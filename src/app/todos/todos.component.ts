import { Component, OnInit, ViewChild } from '@angular/core';
import { TodosService } from './todos.service';
import { TodosInterface } from './todos.interface.component';
import { MatDialog, MatSidenav } from '@angular/material';
import { TodoDeleteComponent } from './dialog/todo-delete/todo.delete.component';
import { TodoEditComponent } from './dialog/todo-edit/todo.edit.component';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.css']
})
export class TodosComponent implements OnInit {

  displayedColumns: string[] = ['id', 'title', 'description', 'created_at', 'updated_at', 'actions'];
  public todos: TodosInterface[] = [];
  public isEmpty = false;
  public isLoading = false;
  public isCreated: string;
  public createForm: FormGroup;
  @ViewChild('sidenav') sidenav: MatSidenav;

  constructor(
    private todosService: TodosService,
    private dialog: MatDialog) {}

  ngOnInit() {
    this.createForm = new FormGroup({
      'title': new FormControl('', Validators.required),
      'description': new FormControl('', Validators.required)
    });
    this.getTodos();
  }

  getTodos(): void {
    this.todosService.getTodos().subscribe(data => {
      this.todos = data;
      if (this.todos.length === 0) {
        this.isEmpty = true;
      } else {
        this.isLoading = true;
      }
    });
  }

  save(): void {
    this.isCreated = 'processing';

    this.todosService.createTodo(this.createForm.value).subscribe((response: Response) => {
      this.isCreated = 'create';
      this.getTodos();
      this.sidenav.close();
    });
  }

  openTodoDeleteDialog(todo: TodosInterface): void {
    const dialogRef = this.dialog.open(TodoDeleteComponent, {
      width: '250px',
      data: { todo: todo },
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.getTodos();
      }
    });
  }

  openTodoEditDialog(todo: TodosInterface): void {
    const dialogRef = this.dialog.open(TodoEditComponent, {
      width: '500px',
      data: { todo: todo },
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.getTodos();
      }
    });
  }

}
