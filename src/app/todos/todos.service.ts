import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

import { environment } from 'src/environments/environment';

const API_URL = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})

export class TodosService {

  constructor(private http: HttpClient) {}

  getTodos(): Observable<any> {
    return this.http.get(`${API_URL}/api/todos`);
  }

  deleteTodo(id: number): Observable<any> {
    return this.http.delete(`${API_URL}/api/todos/${id}`);
  }

  editTodo(id: number, body: any): Observable<any> {
    return this.http.put(`${API_URL}/api/todos/${id}`, body);
  }

  createTodo(body: any): Observable<any> {
    return this.http.post(`${API_URL}/api/todos`, body);
  }
}
